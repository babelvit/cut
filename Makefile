#SHELL = powershell.exe
#.SHELLFLAGS = -Command
#		-vvv > tmp/logs/conan_install.log 2>&1
BUILD = build
BUILD_MGW_RELEASE = $(BUILD)/mingw-release
BUILD_MGW_RELEASE_CONAN = $(BUILD_MGW_RELEASE)/conan
BUILD_MGW_DEBUG = $(BUILD)/mingw-debug
BUILD_MGW_DEBUG_CONAN = $(BUILD_MGW_DEBUG)/conan
BUILD_MSVC = $(BUILD)/msvc
BUILD_MSVC_CONAN = $(BUILD_MSVC)/conan

mingw-release:
	conan install . -of $(BUILD_MGW_RELEASE_CONAN) -pr:h=mingw13 -pr:b=mingw13 --build=missing
	cmake -B $(BUILD_MGW_RELEASE) -G "MinGW Makefiles" -DCMAKE_TOOLCHAIN_FILE=$(BUILD_MGW_RELEASE_CONAN)/conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_POLICY_DEFAULT_CMP0091=NEW

mingw-debug:
	conan install . -of $(BUILD_MGW_DEBUG_CONAN) -pr:h=mingw13 -pr:b=mingw13 -s:h=build_type=Debug -s:b=build_type=Debug --build=missing
	cmake -B $(BUILD_MGW_DEBUG) -G "MinGW Makefiles" -DCMAKE_TOOLCHAIN_FILE=$(BUILD_MGW_DEBUG_CONAN)/conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_POLICY_DEFAULT_CMP0091=NEW

msvc:
	conan install . -of $(BUILD_MSVC_CONAN) -pr:h=msvc -pr:b=msvc -s build_type=Debug --build=missing
	conan install . -of $(BUILD_MSVC_CONAN) -pr:h=msvc -pr:b=msvc -s build_type=Release --build=missing
	cmake -B $(BUILD_MSVC) -G "Visual Studio 17 2022" -DCMAKE_TOOLCHAIN_FILE=$(BUILD_MSVC_CONAN)/conan_toolchain.cmake -DCMAKE_POLICY_DEFAULT_CMP0091=NEW
