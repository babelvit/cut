#pragma once

#include <cstdint>

namespace cut
{
    const uint32_t INT8_BIT = 8;
    const uint32_t FLOAT_BIT = 32;
    const uint32_t DOUBLE_BIT = 64;

    const uint32_t SIGN_BIT = 1;
    const uint32_t FLOAT_EXP_BIT = 8;
    const uint32_t DOUBLE_EXP_BIT = 11;
    const uint32_t FLOAT_MANT_BIT = (FLOAT_BIT - (SIGN_BIT + FLOAT_EXP_BIT));
    const uint32_t DOUBLE_MANT_BIT = (DOUBLE_BIT - (SIGN_BIT + DOUBLE_EXP_BIT));

    const uint32_t FLOAT_SIGN_MASK = (0x01UL << (FLOAT_EXP_BIT + FLOAT_MANT_BIT));
    const uint64_t DOUBLE_SIGN_MASK = (0x01ULL << (DOUBLE_EXP_BIT + DOUBLE_MANT_BIT));
    const uint32_t FLOAT_EXP_MASK = (0xFFUL << FLOAT_MANT_BIT);
    const uint64_t DOUBLE_EXP_MASK = (0x7FFULL << DOUBLE_MANT_BIT);
    const uint32_t FLOAT_MANT_MASK = (0xFFFFFFFFUL >> (SIGN_BIT + FLOAT_EXP_BIT));
    const uint64_t DOUBLE_MANT_MASK = (0xFFFFFFFFFFFFFFFFULL >> (SIGN_BIT + DOUBLE_EXP_BIT));

    /* Zero index returns LSB. */
    template <typename T>
    struct EndianProofDecomposer
    {
        T value;

        uint8_t byteAt(size_t index)
        {
            return (uint8_t)(value >> (index * INT8_BIT));
        }
    };

    template <typename T>
    union Decomposer
    {
    private:
        uint8_t bytes[sizeof(T)];

    public:
        T value;

        uint8_t byteAt(size_t index)
        {
            return bytes[index];
        }
    };

    union FloatDecomposer
    {
    private:
        uint32_t inner;

    public:
        float value;

        uint8_t getSign() { return inner >> (FLOAT_EXP_BIT + FLOAT_MANT_BIT); }
        void setSign(uint8_t sign)
        {
            uint32_t signInPosition = sign;
            signInPosition <<= (FLOAT_EXP_BIT + FLOAT_MANT_BIT);
            inner &= ~FLOAT_SIGN_MASK;
            inner |= signInPosition;
        }

        uint8_t getExponent() { return inner >> FLOAT_MANT_BIT; }
        void setExponent(uint8_t exp)
        {
            uint32_t expInPosition = exp;
            expInPosition <<= FLOAT_MANT_BIT;
            inner &= ~FLOAT_EXP_MASK;
            inner |= expInPosition;
        }

        uint32_t getMantisa() { return inner & FLOAT_MANT_MASK; }
        void setMantisa(uint32_t mant)
        {
            uint32_t mantInPosition = mant & FLOAT_MANT_MASK;
            inner &= ~FLOAT_MANT_MASK;
            inner |= mantInPosition;
        }
    };

    union DoubleDecomposer
    {
    private:
        uint64_t inner;

    public:
        double value;

        uint8_t getSign() { return inner >> (DOUBLE_EXP_BIT + DOUBLE_MANT_BIT); }
        void setSign(uint8_t sign)
        {
            uint64_t signInPosition = sign;
            signInPosition <<= (DOUBLE_EXP_BIT + DOUBLE_MANT_BIT);
            inner &= ~DOUBLE_SIGN_MASK;
            inner |= signInPosition;
        }

        uint16_t getExponent() { return (inner & DOUBLE_EXP_MASK) >> DOUBLE_MANT_BIT; }
        void setExponent(uint16_t exp)
        {
            uint64_t expInPosition = exp;
            expInPosition = (expInPosition << DOUBLE_MANT_BIT) & DOUBLE_EXP_MASK;
            inner &= ~DOUBLE_EXP_MASK;
            inner |= expInPosition;
        }

        uint64_t getMantisa() { return inner & DOUBLE_MANT_MASK; }
        void setMantisa(uint64_t mant)
        {
            uint64_t mantInPosition = mant & DOUBLE_MANT_MASK;
            inner &= ~DOUBLE_MANT_MASK;
            inner |= mantInPosition;
        }
    };
}
