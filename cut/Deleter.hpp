#pragma once

#include <cstdlib>

namespace cut
{
    struct FreeDeleter
    {
        void operator()(void* p) { std::free(p); }
    };
}