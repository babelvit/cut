#pragma once

#include <type_traits>
#include <iterator>

namespace cut
{
    #define CUT_IS_COMPILABLE(NAME, DECLARE, EXPRESSION)                                                    \
    template<typename type_> struct NAME {                                                                  \
    private:                                                                                                \
		DECLARE                                                                                             \
        typedef char yes;                                                                                   \
        typedef char no[2];                                                                                 \
        template<typename T> static yes& test(T instance, decltype(EXPRESSION, int()) a = 0);               \
        template<typename T> static no& test(...);                                                          \
    public:                                                                                                 \
        static constexpr bool value = sizeof(NAME::test<type_>(std::declval<type_>())) == sizeof(NAME::yes);\
    };

    template <typename type_>
    struct remove_all_pointers {
        typedef type_ type;
    };

    template <typename type_>
    struct remove_all_pointers<type_*> {
        typedef typename remove_all_pointers<type_>::type type;
    };

    template <typename type_>
    struct remove_all_pointers<type_**> {
        typedef typename remove_all_pointers<type_>::type type;
    };

    template<typename type_>
    struct is_const
    {
        static constexpr bool value = std::is_const<
            typename remove_all_pointers<
                typename std::remove_reference<type_>::type
            >::type
        >::value;
    };

    template<typename iterator_type>
    struct iterator_info
    {
        typedef typename std::remove_reference<
            typename std::iterator_traits<iterator_type>::reference
        >::type return_type;
        static constexpr bool is_const = is_const<return_type>::value;
    };

    template<typename func_type>
    struct result_is_const
    {
        static constexpr bool value = is_const<
            typename std::result_of<func_type>::type
        >::value;
    };

    CUT_IS_COMPILABLE(is_dereferecable, , *instance);
    CUT_IS_COMPILABLE(is_iterable, , ++instance);
    CUT_IS_COMPILABLE(is_comparable, , instance == instance);

    template<typename type_>
    struct is_iterator
    {
        static constexpr bool value = is_dereferecable<type_>::value
            && is_iterable<type_>::value
            && is_comparable<type_>::value;
    };

    CUT_IS_COMPILABLE(has_begin, , std::begin(instance));
    CUT_IS_COMPILABLE(has_end, , std::end(instance));

    template<typename type_>
    struct is_range
    {
        static constexpr bool value = has_begin<type_>::value && has_end<type_>::value;
    };

    CUT_IS_COMPILABLE(has_getline, char* dummy;, instance.getline(dummy, 0));
	
    template<typename type_>
    struct is_istream
    {
        static constexpr bool value = has_getline<type_>::value;
    };
}
