#pragma once

#include <functional>
#include <optional>

namespace cut {

template <typename T>
class lazy {
private:
    std::function<T()> create;
    std::optional<T> storage;

public:
    lazy(std::function<T()>&& create) : create(std::move(create)) {}

    lazy<T>& operator=(std::function<T()>&& create) {
        this->create = std::move(create);
        return *this;
    }

    const T& get() {
        if (!storage.has_value()) {
            storage = create();
        }
        return storage.value();
    }
};

}
