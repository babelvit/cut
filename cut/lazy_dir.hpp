#pragma once

#include <filesystem>
#include <functional>
#include <optional>

namespace cut {

class lazy_dir {
private:
    std::function<std::filesystem::path()> create;
    std::optional<std::filesystem::path> directory;

public:
    lazy_dir(std::function<std::filesystem::path()>&& create) : create(std::move(create)) {}

    lazy_dir& operator=(std::function<std::filesystem::path()>&& create) {
        this->create = std::move(create);
        return *this;
    }

    const std::filesystem::path& get() {
        if (!directory.has_value()) {
            directory = create();
            if (!std::filesystem::exists(*directory)) {
                std::filesystem::create_directory(*directory);
            }
        }
        return directory.value();
    }
};

}
