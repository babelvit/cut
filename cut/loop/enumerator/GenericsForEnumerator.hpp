#pragma once

#define CUT_ENUMERATOR_TYPEDEFS(ClassName, IteratorName)                                        \
typedef std::forward_iterator_tag iterator_category;                                            \
typedef typename std::iterator_traits<IteratorName>::difference_type difference_type;           \
typedef typename std::conditional<TIsConst, const value_type&, value_type&>::type reference;    \
typedef typename std::conditional<TIsConst, const value_type*, value_type*>::type pointer;      \
static constexpr bool is_const = TIsConst;

#define CUT_ENUMERATOR_OPERATORS(ClassName)                                                     \
ClassName& operator++() { move_next(); return *this; }                                          \
ClassName operator++(int) { ClassName tmp(*this); operator++(); return tmp; }                   \
/* HACK: Does not compare! */                                                                   \
bool operator==(const ClassName& dummy) const { return is_finished(); }                         \
/* HACK: Does not compare! */                                                                   \
bool operator!=(const ClassName& dummy) const { return !is_finished(); }                        \
reference operator*() const { return get_current(); }                                           \
pointer operator->() const { return &get_current(); }
