#pragma once

#include <iterator>

#include "../../Metafunc.hpp"
#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <typename TIterator, bool TIsConst = iterator_info<TIterator>::is_const>
    class iterator_adapter
    {
    public:
        typedef iterator_adapter<TIterator, true> const_variant;
        typedef typename std::iterator_traits<TIterator>::value_type value_type;
        CUT_ENUMERATOR_TYPEDEFS(iterator_adapter, TIterator)

    private:
        TIterator _current;
        TIterator _end;

    public:
        iterator_adapter(TIterator& begin, TIterator& end)
            : _current(std::move(begin))
            , _end(std::move(end))
        { }

        iterator_adapter() {}

        reference get_current() const
        {
            return *_current;
        }

        bool is_finished() const
        {
            return _current == _end;
        }

        void move_next()
        {
            ++_current;
        }

        CUT_ENUMERATOR_OPERATORS(iterator_adapter)
    };

    template <typename TIterator>
    inline static iterator_adapter<TIterator> make_iterator_adapter(TIterator& begin, TIterator& end)
    {
        return iterator_adapter<TIterator>(begin, end);
    }
}
