#pragma once

#include <iterator>

#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <class TEnumerator, bool TIsConst = TEnumerator::is_const>
    class skip_enumerator
    {
    public:
        typedef skip_enumerator<TEnumerator, true> const_variant;
        typedef typename std::iterator_traits<TEnumerator>::value_type value_type;
        CUT_ENUMERATOR_TYPEDEFS(skip_enumerator, TEnumerator)

    private:
        TEnumerator _enumerator;
        int32_t _count;

        void moveFirst()
        {
            while (!_enumerator.is_finished() && _count > 0)
            {
                --_count;
                _enumerator.move_next();
            }
        }

    public:
        skip_enumerator(TEnumerator& enumerator, int32_t count)
            : _enumerator(std::move(enumerator))
            , _count(count)
        {
            moveFirst();
        }

        skip_enumerator() {}

        reference get_current() const
        {
            return _enumerator.get_current();
        }

        bool is_finished() const
        {
            return _enumerator.is_finished();
        }

        void move_next()
        {
            _enumerator.move_next();
        }

        CUT_ENUMERATOR_OPERATORS(skip_enumerator)
    };

    template <typename TEnumerator>
    inline static skip_enumerator<TEnumerator> make_skip(TEnumerator& enumerator, int32_t count)
    {
        return skip_enumerator<TEnumerator>(enumerator, count);
    }
}