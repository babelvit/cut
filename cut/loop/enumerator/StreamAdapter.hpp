#pragma once

#include <iterator>
#include <string>

#include "../../Metafunc.hpp"
#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <typename TStream, bool TIsConst = false>
    class StreamAdapter
    {
    public:
        typedef StreamAdapter<TStream, true> const_variant;
        typedef typename std::string value_type;

        typedef std::forward_iterator_tag iterator_category;
        typedef int difference_type;
        typedef typename std::conditional<TIsConst, const value_type&, value_type&>::type reference;
        typedef typename std::conditional<TIsConst, const value_type*, value_type*>::type pointer;
        static constexpr bool is_const = TIsConst;

    private:
        std::string _current_line;
        bool _end_of_stream = false;
        TStream& _stream;

    public:
        StreamAdapter(TStream& stream)
            : _stream(stream)
        {
            move_next();
        }

        StreamAdapter() {}

        reference get_current()
        {
            return _current_line;
        }

        bool is_finished() const
        {
            return _end_of_stream;
        }

        void move_next()
        {
            _end_of_stream = _stream.eof();
            std::getline(_stream, _current_line);
        }

        CUT_ENUMERATOR_OPERATORS(StreamAdapter)
    };

    template <typename TStream>
    inline static StreamAdapter<TStream> make_stream_adapter(TStream& stream)
    {
        return StreamAdapter<TStream>(stream);
    }
}
