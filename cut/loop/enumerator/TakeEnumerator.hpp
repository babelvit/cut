#pragma once

#include <iterator>

#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <typename TEnumerator, bool TIsConst = TEnumerator::is_const>
    class take_enumerator
    {
    public:
        typedef take_enumerator<TEnumerator, true> const_variant;
        typedef typename std::iterator_traits<TEnumerator>::value_type value_type;
        CUT_ENUMERATOR_TYPEDEFS(take_enumerator, TEnumerator)

    private:
        TEnumerator _enumerator;
        int32_t _count;

    public:
        take_enumerator(TEnumerator& enumerator, int32_t count)
            : _enumerator(std::move(enumerator))
            , _count(count)
        { }

        take_enumerator() {}

        reference get_current() const
        {
            return _enumerator.get_current();
        }

        bool is_finished() const
        {
            return _count <= 0 || _enumerator.is_finished();
        }

        void move_next()
        {
            --_count;
            _enumerator.move_next();
        }

        CUT_ENUMERATOR_OPERATORS(take_enumerator)
    };

    template <typename TEnumerator>
    inline static take_enumerator<TEnumerator> make_take(TEnumerator& enumerator, int32_t count)
    {
        return take_enumerator<TEnumerator>(enumerator, count);
    }
}