#pragma once

#include <type_traits>
#include <iterator>

#include "../../Metafunc.hpp"
#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <typename TEnumerator, typename TTransformer,
        bool TIsConst = result_is_const<TTransformer(typename TEnumerator::reference)>::value
    >
    class transform_enumerator
    {
    public:
        typedef transform_enumerator<TEnumerator, TTransformer, true> const_variant;
        typedef typename std::remove_pointer<
            typename std::result_of<TTransformer(typename TEnumerator::reference)>::type
        >::type value_type;
        CUT_ENUMERATOR_TYPEDEFS(transform_enumerator, TEnumerator)

    private:
        TEnumerator _enumerator;
        TTransformer _transformer;

    public:
        transform_enumerator(TEnumerator& enumerator, TTransformer& transform)
            : _enumerator(std::move(enumerator))
            , _transformer(transform)
        { }

        reference get_current()
        {
            return *_transformer(_enumerator.get_current());
        }

        bool is_finished() const
        {
            return _enumerator.is_finished();
        }

        void move_next()
        {
            _enumerator.move_next();
        }

        CUT_ENUMERATOR_OPERATORS(transform_enumerator)
    };

    template <typename TEnumerator, typename TTransformer>
    inline static transform_enumerator<TEnumerator, TTransformer> make_transform(TEnumerator& enumerator, TTransformer& transformer)
    {
        return transform_enumerator<TEnumerator, TTransformer>(enumerator, transformer);
    }
}
