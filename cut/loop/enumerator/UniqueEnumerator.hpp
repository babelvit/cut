#pragma once

#include <iterator>
#include <type_traits>
#include <set>

#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <class TEnumerator, typename TTransformer, bool TIsConst = TEnumerator::is_const>
    class unique_enumerator
    {
    public:
        typedef unique_enumerator<TEnumerator, TTransformer, true> const_variant;
        typedef typename std::iterator_traits<TEnumerator>::value_type value_type;
        CUT_ENUMERATOR_TYPEDEFS(unique_enumerator, TEnumerator)
        typedef typename std::remove_reference<
            typename std::result_of<TTransformer(typename TEnumerator::reference)>::type
        >::type key_type;

    private:
        TEnumerator _enumerator;
        TTransformer _transformer;
        std::set<key_type> _uniqueKeys;
        uint32_t _lastUniqueKeyCount;
        pointer _current;

        void bringCurrent()
        {
            _current = &_enumerator.get_current();
            _uniqueKeys.insert(_transformer(*_current));
        }

        bool currentShouldBeFiltered()
        {
            bringCurrent();
            return _uniqueKeys.size() == _lastUniqueKeyCount
                ? true
                : (++_lastUniqueKeyCount, false);
        }

    public:
        unique_enumerator(TEnumerator& enumerator, TTransformer& transformer)
            : _enumerator(std::move(enumerator))
            , _transformer(transformer)
        {
            if (!_enumerator.is_finished())
            {
                bringCurrent();
                _lastUniqueKeyCount = 1;
            }
        }

        reference get_current() const
        {
            return *_current;
        }

        bool is_finished() const
        {
            return _enumerator.is_finished();
        }

        void move_next()
        {
            do
            {
                _enumerator.move_next();
            } while (!_enumerator.is_finished() && currentShouldBeFiltered());
        }

        CUT_ENUMERATOR_OPERATORS(unique_enumerator)
    };

    template <typename TEnumerator, typename TTransformer>
    inline static unique_enumerator<TEnumerator, TTransformer>
        make_unique(TEnumerator& enumerator, TTransformer& selector)
    {
        return unique_enumerator<TEnumerator, TTransformer>(enumerator, selector);
    }
}