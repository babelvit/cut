#pragma once

#include <iterator>

#include "GenericsForEnumerator.hpp"

namespace cut
{
    template <class TEnumerator, typename TPredicate, bool TIsConst = TEnumerator::is_const>
    class where_enumerator
    {
    public:
        typedef where_enumerator<TEnumerator, TPredicate, true> const_variant;
        typedef typename std::iterator_traits<TEnumerator>::value_type value_type;
        CUT_ENUMERATOR_TYPEDEFS(where_enumerator, TEnumerator)

    private:
        TEnumerator _enumerator;
        TPredicate& _predicate;
        pointer _current;

        void moveFirst()
        {
            move_next(true);
        }

        void move_next(bool toFirst)
        {
            if(!toFirst)
                _enumerator.move_next();

            while (!_enumerator.is_finished() && currentShouldBeFiltered())
            {
                _enumerator.move_next();
            }
        }

        bool currentShouldBeFiltered()
        {
            _current = &_enumerator.get_current();
            return !_predicate(*_current);
        }

    public:
        where_enumerator(TEnumerator& enumerator, TPredicate& predicate)
            : _enumerator(std::move(enumerator))
            , _predicate(predicate)
        {
            moveFirst();
        }

        reference get_current() const
        {
            return *_current;
        }

        bool is_finished() const
        {
            return _enumerator.is_finished();
        }

        void move_next()
        {
            move_next(false);
        }

        CUT_ENUMERATOR_OPERATORS(where_enumerator)
    };

    template <typename TEnumerator, typename TPredicate>
    inline static where_enumerator<TEnumerator, TPredicate> make_where(TEnumerator& enumerator, TPredicate& predicate)
    {
        return where_enumerator<TEnumerator, TPredicate>(enumerator, predicate);
    }
}