#pragma once

#include <numeric>
#include <type_traits>

#include "./enumerator/IteratorAdapter.hpp"
#include "./enumerator/StreamAdapter.hpp"
#include "./enumerator/TransformEnumerator.hpp"
#include "./enumerator/TakeEnumerator.hpp"
#include "./enumerator/SkipEnumerator.hpp"
#include "./enumerator/WhereEnumerator.hpp"
#include "./enumerator/UniqueEnumerator.hpp"

#include "cut/Metafunc.hpp"

namespace cut {

const bool CONTINUE = true;
const bool BREAK = false;

template <typename TEnumerator>
class methods
{
public:
    typedef TEnumerator iterator;
    typedef typename TEnumerator::const_variant const_iterator;

private:
    TEnumerator _enumerator;

public:
    methods(TEnumerator&& enumerator) : _enumerator(std::move(enumerator)) {}

    template <typename TTransformer>
    methods<transform_enumerator<TEnumerator, TTransformer> > transform(TTransformer transformer) {
        return methods<transform_enumerator<TEnumerator, TTransformer> >(
            make_transform(_enumerator, transformer)
        );
    }

    methods<take_enumerator<TEnumerator> > take(int32_t count) {
        return methods<take_enumerator<TEnumerator> >(make_take(_enumerator, count));
    }

    methods<skip_enumerator<TEnumerator> > skip(int32_t count) {
        return methods<skip_enumerator<TEnumerator> >(make_skip(_enumerator, count));
    }

    template <typename TPredicate>
    methods<where_enumerator<TEnumerator, TPredicate> > where(TPredicate predicate) {
        return methods<where_enumerator<TEnumerator, TPredicate> >(make_where(_enumerator, predicate));
    }

private:
    template <typename T>
    struct dummy_transformer {
        T& operator()(T& a) const { return a; }
    };

public:
    methods<unique_enumerator<TEnumerator, dummy_transformer<typename TEnumerator::reference> > >
    unique() {
        dummy_transformer<typename TEnumerator::reference> transformer;
        return unique(transformer);
    }

    template <typename TTransformer>
    methods<unique_enumerator<TEnumerator, TTransformer> > unique(TTransformer transformer) {
        return methods<unique_enumerator<TEnumerator, TTransformer> >(
            make_unique(_enumerator, transformer)
        );
    }

    // Exporters

    iterator& begin() { return _enumerator; }

    iterator& end() { return _enumerator; }

    const_iterator& begin() const {
        return reinterpret_cast<const_iterator&>(const_cast<iterator&>(_enumerator));
    }

    const_iterator& end() const {
        return reinterpret_cast<const_iterator&>(const_cast<iterator&>(_enumerator));
    }

    template<typename TOStream>
    void write_to(TOStream& stream, const typename TOStream::char_type* delimiter = "") {
        foreach([&](typename TEnumerator::reference item)
        {
            stream << item << delimiter;
        });
    }

    template<typename TIterator>
    void copy_to(TIterator current) {
        foreach([&current](typename TEnumerator::reference item)
        {
            *current = item;
            ++current;
        });
    }

    template<typename TIterator>
    void move_to(TIterator current) {
        foreach([&current](typename TEnumerator::reference item)
        {
            *current = std::move(item);
            ++current;
        });
    }

    template<typename TCollection>
    void copy_back_to(TCollection& collection) {
        foreach([&collection](typename TEnumerator::reference item)
        {
            collection.push_back(item);
        });
    }

    template<typename TCollection>
    void move_back_to(TCollection& collection) {
        foreach([&collection](typename TEnumerator::reference item)
        {
            collection.push_back(std::move(item));
        });
    }

    // sum

    template<typename TRet = typename TEnumerator::value_type>
    TRet sum(TRet addend = typename TEnumerator::value_type())
    {
        return sum_if(addend, [](typename TEnumerator::reference i) { return true; });
    }

    template<typename TPredicate>
    typename TEnumerator::value_type sum_if(TPredicate predicate)
    {
        return sum_if(typename TEnumerator::value_type(), predicate);
    }

    template<typename TRet, typename TPredicate>
    TRet sum_if(TRet addend, TPredicate predicate)
    {
        return std::accumulate(_enumerator, _enumerator, addend,
            [=](TRet accumulator, typename TEnumerator::value_type i) {
                return accumulator + (predicate(i) ? i : 0);
            }
        );
    }

    // foreach

    template<typename TLoop>
    typename std::enable_if<std::is_void<
        typename std::result_of<TLoop(typename TEnumerator::reference)>::type
    >::value>::type
    foreach(TLoop loop) {
        for (; !_enumerator.is_finished(); _enumerator.move_next())
        {
            typename TEnumerator::reference ret = _enumerator.get_current();
            loop(ret);
        }
    }

    template<typename TLoop>
    typename std::enable_if<std::is_same<
        typename std::result_of<TLoop(typename TEnumerator::reference)>::type,
        bool
    >::value>::type
    foreach(TLoop loop) {
        for (bool keepGoing = true; keepGoing && !_enumerator.is_finished(); _enumerator.move_next())
        {
            typename TEnumerator::reference ret = _enumerator.get_current();
            keepGoing = loop(ret);
        }
    }
};

template<typename TIterator>
methods<iterator_adapter<TIterator> >
loop(TIterator begin, TIterator end) {
    return methods<iterator_adapter<TIterator> >(make_iterator_adapter(begin, end));
}

template<typename TCollection>
auto loop(TCollection&& collection)
-> decltype(loop(std::begin(collection), std::end(collection))) {
    return loop(std::begin(collection), std::end(collection));
}

template<typename TValue, size_t N>
methods<iterator_adapter<TValue*> >
loop(TValue(&array)[N]) {
    return loop(array, array + N);
}

template<typename TCollection>
auto loop_reversed(TCollection& collection)
-> decltype(loop(std::rbegin(collection), std::rend(collection))) {
    return loop(std::rbegin(collection), std::rend(collection));
}

template<typename TStream>
methods<StreamAdapter<TStream> >
loop_stream(TStream& stream) {
    return methods<StreamAdapter<TStream> >(make_stream_adapter(stream));
}

}
