#pragma once

namespace cut {

struct life_log {
    int cstr_count;
    int copy_count;
    int move_count;
    int dstr_count;

private:
    bool is_started;

public:
    life_log()
        : cstr_count(0)
        , copy_count(0)
        , move_count(0)
        , dstr_count(0)
        , is_started(false)
    {}

    void start() { is_started = true; }
    void stop() { is_started = false; }
    void on_construct() { if (is_started) { ++cstr_count; } }
    void on_copy() { if (is_started) { ++copy_count; } }
    void on_move() { if (is_started) { ++move_count; } }
    void on_destruct() { if (is_started) { ++dstr_count; } }

    friend std::ostream& operator<< (std::ostream& os, const life_log& log) {
        return os << "life_log(cstr: " << log.cstr_count <<
            ", copy: " << log.copy_count <<
            ", move: " << log.move_count <<
            ", dstr: " << log.dstr_count <<
        ")";
    }
};

class life_logger {
private:
    life_log& log;
    int value;

public:
    life_logger(life_log& log, int value = 0) : log(log), value(value) { log.on_construct(); }
    life_logger(const life_logger& other) : log(other.log), value(other.value) { log.on_copy(); }
    life_logger(life_logger&& other) : log(other.log), value(other.value) { log.on_move(); }
    ~life_logger() { log.on_destruct(); }

    life_logger& operator=(life_logger&& other) {
        log.on_move();
        if (this != &other) { log = other.log; }
        return *this;
    }
    bool operator< (const life_logger& other) const { return value < other.value; }
    bool operator> (const life_logger& other) const { return value > other.value; }
    friend std::ostream& operator<< (std::ostream& os, const life_logger& logger) {
        return os << "life_logger(" << logger.log << ")";
    }
};

}
