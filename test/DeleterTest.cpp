#define BOOST_TEST_MODULE CutTestModule
#include <memory>
#include <cstdlib>
#include <cut/Deleter.hpp>
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(DeleterTS)

BOOST_AUTO_TEST_CASE(testFreeMalloc)
{
    std::unique_ptr<void, cut::FreeDeleter> ptr(std::malloc(sizeof(int)));
    BOOST_CHECK(ptr.get());
}

BOOST_AUTO_TEST_SUITE_END()
