#include <filesystem>
#include <functional>
#include <boost/test/unit_test.hpp>
#include "cut/lazy_dir.hpp"

BOOST_AUTO_TEST_SUITE(LazyDir)

namespace fs = std::filesystem;

class Config
{
public:
    cut::lazy_dir tmp1_dir = std::function([this]() { return fs::path("tmp1"); });
    cut::lazy_dir tmp2_dir = std::function([this]() { return tmp1_dir.get() / "tmp2"; });

    ~Config() {
        fs::remove_all(tmp1_dir.get());
    }
};

BOOST_AUTO_TEST_CASE(ShouldCreateDirectory)
{
    fs::path tmp2_dir;
    {
        Config config;
        tmp2_dir = config.tmp2_dir.get();
        BOOST_CHECK(fs::exists(tmp2_dir));
    }
    BOOST_CHECK(!fs::exists(tmp2_dir));
}

BOOST_AUTO_TEST_SUITE_END()
