#include <filesystem>
#include <functional>
#include <string>
#include <sstream>
#include <boost/test/unit_test.hpp>
#include "cut/lazy.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Lazy)

class Config
{
private:
    cut::life_log &log;

public:
    cut::lazy<cut::life_logger> logger = std::function([this]() {
        return cut::life_logger(log);
    });

    cut::lazy<std::string> logger_str = std::function([this]() {
        std::stringstream ss;
        ss << logger.get();
        return ss.str();
    });

    Config(cut::life_log &log) : log(log) {}
};

BOOST_AUTO_TEST_CASE(ShouldConstructObjectJustOnce)
{
    cut::life_log log;
    log.start();
    {
        Config config(log);
        auto& logger1 = config.logger.get();
        auto& logger2 = config.logger.get();

        BOOST_CHECK_EQUAL(&logger1, &logger2);

        BOOST_CHECK_EQUAL(log.cstr_count, 1);
        BOOST_CHECK_EQUAL(log.move_count, 1);
        BOOST_CHECK_EQUAL(log.copy_count, 0);
        BOOST_CHECK_EQUAL(log.dstr_count, 1);
    }
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 1);
    BOOST_CHECK_EQUAL(log.move_count, 1);
    BOOST_CHECK_EQUAL(log.copy_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 2);
}

BOOST_AUTO_TEST_CASE(ShouldResetConstruction)
{
    cut::life_log log1;
    cut::life_log log2;

    log1.start();
    log2.start();
    
    Config config(log1);
    config.logger = [&]() {
        return cut::life_logger(log2);
    };

    auto& logger = config.logger.get();

    BOOST_CHECK_EQUAL(log1.cstr_count, 0);
    BOOST_CHECK_EQUAL(log2.cstr_count, 1);
}

BOOST_AUTO_TEST_CASE(ShouldChainContructions)
{
    cut::life_log log;
    log.start();
    Config config(log);

    auto str = config.logger_str.get();

    BOOST_CHECK_EQUAL(log.cstr_count, 1);
}

BOOST_AUTO_TEST_SUITE_END()
