#include "cut/Metafunc.hpp"
#include <string>
#include <vector>
#include <sstream>
#include <iostream>

/*
	Static testing runs during compilation
*/

struct functor_return_const
{
    const int& operator()(int& i) { return i; }
};

struct functor_return_mutable
{
    int& operator()(int& i) { return i; }
};

void test_is_iterator()
{
    static_assert(!cut::is_iterator<int>::value, "test_is_iterator");
    static_assert(cut::is_iterator<int*>::value, "test_is_iterator");
    static_assert(
		cut::is_iterator<std::vector<int>::iterator>::value,
		"test_is_iterator");
}

void test_is_range()
{
    static_assert(!cut::is_range<int>::value, "test_is_range");
    static_assert(cut::is_range<std::vector<int> >::value, "test_is_range");
}

void test_is_stream()
{
    static_assert(!cut::is_istream<int>::value, "test_is_stream");
    static_assert(cut::is_istream<std::stringstream>::value, "test_is_stream");
}

void test_remove_all_pointers()
{
    static_assert(
        std::is_same<typename cut::remove_all_pointers<int>::type, int>::value,
        "test_remove_all_pointers");
    static_assert(
        std::is_same<typename cut::remove_all_pointers<int**>::type, int>::value,
        "test_remove_all_pointers");
    static_assert(
        std::is_same<typename cut::remove_all_pointers<int****>::type, int>::value,
        "test_remove_all_pointers");
}

void test_result_is_const()
{
    static_assert(
		cut::result_is_const<functor_return_const(int&)>::value,
		"test_result_is_const");
    static_assert(
		!cut::result_is_const<functor_return_mutable(int&)>::value,
		"test_result_is_const");
}

void test_is_const()
{
    static_assert(cut::is_const<const int>::value, "test_is_const");
    static_assert(cut::is_const<const int&>::value, "test_is_const");
    static_assert(cut::is_const<const int&&>::value, "test_is_const");
    static_assert(cut::is_const<const int*>::value, "test_is_const");
    static_assert(cut::is_const<const int**>::value, "test_is_const");
    static_assert(!cut::is_const<int>::value, "test_is_const");
    static_assert(!cut::is_const<int&>::value, "test_is_const");
    static_assert(!cut::is_const<int&&>::value, "test_is_const");
    static_assert(!cut::is_const<int*>::value, "test_is_const");
    static_assert(!cut::is_const<int**>::value, "test_is_const");
}
