#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/iterator_range.hpp>
#include <memory>
#include <vector>
#include "cut/loop/loop.hpp"

namespace range = boost::adaptors;

BOOST_AUTO_TEST_SUITE(Compatibility)

BOOST_AUTO_TEST_CASE(testNonCopyables)
{
    std::vector<std::unique_ptr<int> > input;
    input.emplace_back(std::unique_ptr<int>(new int(1)));
    input.emplace_back(std::unique_ptr<int>(new int(2)));
    input.emplace_back(std::unique_ptr<int>(new int(3)));

    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input)
        .transform([](const auto& uniquePtr) { return uniquePtr.get(); })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromRange)
{
    std::vector<int> input{ 1, 2, 3, 3 };
    std::vector<int> result, expected{ 3, 2, 1 };

	cut::loop(input | range::reversed | range::uniqued).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testLoopToRange)
{
    std::vector<int> input{ 1, 2, 3, 3 };
    std::vector<int> result, expected{ 3, 2, 1 };

    boost::push_back(result, cut::loop_reversed(input) | range::uniqued);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testSkiptToRange)
{
    std::vector<int> input{ 1, 2, 3, 3 };
    std::vector<int> result, expected{ 2, 3 };

    boost::push_back(result, cut::loop(input).skip(1) | range::uniqued);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testTakeToRange)
{
    std::vector<int> input{ 1, 2, 3, 3 };
    std::vector<int> result, expected{ 1, 2, 3 };

    boost::push_back(result, cut::loop(input).take(4) | range::uniqued);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_SUITE_END()
