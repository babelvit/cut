#include <string>
#include <vector>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"

BOOST_AUTO_TEST_SUITE(Foreach)

BOOST_AUTO_TEST_CASE(testForeachDoTransform)
{
    std::vector<int> input { 1, 2, 3 };
    std::vector<std::string> result, expected { "1", "2", "3" };

    cut::loop(input).foreach([&result](int i)
    {
        result.emplace_back(std::to_string(i));
    });

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testForeachIndexWhereTakeSelecet)
{
    std::vector<std::string> input { "1", "2", "3", "1", "2", "3" };
    std::vector<int> result, expected { 1, 2, 1 };

    size_t i = 0;
    cut::loop(input).foreach([&result, &i](const std::string& str)
    {
        int sum = std::stoi(str); // transform(str -> int)
        if (sum % 3 == 0) return cut::CONTINUE; // where(sum % 3 != 0)
        if (++i > 3) return cut::BREAK; // take(3)

        result.push_back(sum);
        return cut::CONTINUE;
    });

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_SUITE_END()
