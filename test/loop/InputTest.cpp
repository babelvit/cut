#include <boost/test/unit_test.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include <list>
#include <deque>
#include <array>
#include <map>
#include <string>
#include "cut/loop/loop.hpp"

BOOST_AUTO_TEST_SUITE(Input)

BOOST_AUTO_TEST_CASE(testFromIterators)
{
    const std::vector<int> input { 1, 2, 3 };
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input.cbegin(), input.cend()).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromList)
{
    const std::list<int> input { 1, 2, 3 };
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromStackArray)
{
    const int input[3] { 1, 2, 3 };
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromHeapArray)
{
    size_t N = 3;
    std::unique_ptr<const int[]> input(new const int[N] { 1, 2, 3 });
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input.get(), input.get() + N).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromStdArray)
{
    const std::array<int, 3> input { 1, 2, 3 };
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromMap)
{
    const std::map<int, int> input { { 1, 1 },{ 2, 2 },{ 3, 3 } };
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input).transform([](const auto& pair) { return &pair.second; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromReversed)
{
    const std::list<int> input{ 1, 2, 3 };
    std::vector<int> result, expected{ 3, 2, 1 };

    cut::loop_reversed(input).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testFromStreamLines)
{
    std::stringstream input{"1\n2\n3"};
    std::vector<std::string> result, expected{ "1", "2", "3" };

    cut::loop_stream(input).move_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());

    input = std::stringstream{ };
    result = { }, expected = {""};

    cut::loop_stream(input).move_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_SUITE_END()
