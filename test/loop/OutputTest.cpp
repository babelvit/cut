#include <boost/test/unit_test.hpp>
#include <vector>
#include <string>
#include <sstream>
#include <list>
#include <deque>

#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Output)

BOOST_AUTO_TEST_CASE(testPushBackToDeque)
{
    std::vector<int> input { 1, 2, 3 };
    std::deque<int> result, expected { 1, 2, 3 };

    cut::loop(input).copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testEmplaceBackToList)
{
    std::vector<std::string> input{ "1", "2", "3" }, inputResult{"", "", ""};
    std::list<std::string> result, expected{ "1", "2", "3" };

    cut::loop(input).move_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(inputResult.begin(), inputResult.end(), input.begin(), input.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testCopyToString)
{
    std::vector<char> input{ 'a', 's', 'd', 'f' };
    std::string result("----"), expected("asdf");

    cut::loop(input).copy_to(result.begin());

    BOOST_CHECK_EQUAL(result, expected);
}

BOOST_AUTO_TEST_CASE(testMoveToVector)
{
    std::vector<std::string> input{ "1", "2", "3" };
    std::vector<std::string> inputResult{ "", "", "" };
    std::vector<std::string> result{ "-", "-", "-" };
    std::vector<std::string> expected{ "1", "2", "3" };

    cut::loop(input).move_to(result.begin());

    BOOST_CHECK_EQUAL_COLLECTIONS(inputResult.begin(), inputResult.end(), input.begin(), input.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testWriteToStream)
{
    std::vector<std::string> input{ "1", "2", "3" };
    std::ostringstream result;
    std::string expected("1, 2, 3, ");

    cut::loop(input).write_to(result, ", ");

    BOOST_CHECK_EQUAL(result.str(), expected);
}

BOOST_AUTO_TEST_SUITE_END()
