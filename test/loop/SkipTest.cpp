#include <vector>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Skip)

BOOST_AUTO_TEST_CASE(testSkipFour)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { 5, 6, 7, 8 };

    cut::loop(input)
        .skip(4)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testMultipleSkip)
{
    std::vector<int> input{ 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected{ 7, 8 };

    cut::loop(input)
        .skip(2)
        .skip(2)
        .skip(2)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testSkipZero)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected = input;

    cut::loop(input)
        .skip(0)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testSkipNegativeNumber)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected = input;

    cut::loop(input.begin(), input.end())
        .skip(-3)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testSkipMoreThanLength)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected{ };

    cut::loop(input)
        .skip(11)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testCopyCount)
{
    cut::life_log log;

    std::vector<cut::life_logger> input;
    input.reserve(5);
    input.emplace_back(log);
    input.emplace_back(log);
    input.emplace_back(log);

    std::vector<cut::life_logger> result;
    result.reserve(5);

    log.start();
    cut::loop(input).skip(-1).skip(0).skip(1).copy_back_to(result);
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 0);
    BOOST_CHECK_EQUAL(log.copy_count, 2);
    BOOST_CHECK_EQUAL(log.move_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 0);
}

BOOST_AUTO_TEST_SUITE_END()
