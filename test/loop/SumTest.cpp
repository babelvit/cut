#include <vector>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"

BOOST_AUTO_TEST_SUITE(Sum)

BOOST_AUTO_TEST_CASE(testSimple)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    int result, expected = 36;

    result = cut::loop(input).sum();

    BOOST_CHECK_EQUAL(result, expected);
}

BOOST_AUTO_TEST_CASE(testSelective)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    int result, expected = 20;

    result = cut::loop(input).sum_if([](int i) { return i % 2 == 0; });

    BOOST_CHECK_EQUAL(result, expected);
}

BOOST_AUTO_TEST_CASE(testToDouble)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    double result, expected = 36.1;

    result = cut::loop(input).sum(double(0.1));

    BOOST_CHECK_EQUAL(result, expected);
}

BOOST_AUTO_TEST_SUITE_END()
