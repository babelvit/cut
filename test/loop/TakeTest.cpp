#include <vector>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Take)

BOOST_AUTO_TEST_CASE(testTakeFour)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { 1, 2, 3, 4 };

    cut::loop(input)
        .take(4)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testMultipleTake)
{
    std::vector<int> input{ 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected{ 1, 2 };

    cut::loop(input)
        .take(6)
        .take(4)
        .take(2)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testTakeZero)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { };

    cut::loop(input)
        .take(0)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testTakeNegativeNumber)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { };

    cut::loop(input.begin(), input.end())
        .take(-3)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testTakeMoreThanLength)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected = input;

    cut::loop(input)
        .take(11)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testCopyCount)
{
    cut::life_log log;

    std::vector<cut::life_logger> input;
    input.reserve(5);
    input.emplace_back(log);
    input.emplace_back(log);
    input.emplace_back(log);

    std::vector<cut::life_logger> result;
    result.reserve(5);

    log.start();
    cut::loop(input).take(11).take(10).take(9).copy_back_to(result);
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 0);
    BOOST_CHECK_EQUAL(log.copy_count, 3);
    BOOST_CHECK_EQUAL(log.move_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 0);
}

BOOST_AUTO_TEST_SUITE_END()
