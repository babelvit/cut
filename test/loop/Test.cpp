#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"

BOOST_AUTO_TEST_SUITE(Loop)

BOOST_AUTO_TEST_CASE(testWhereTakeSelecetPipeline)
{
    std::vector<std::pair<int, int> > input {{ 1, 1 },{ 2, 2 },{ 3, 3 },{ 1, 1 },{ 2, 2 },{ 3, 3 }};
    std::vector<int> result, expected { 2, 4, 2 };

    int storage;
    cut::loop(input)
        .transform([&storage](const auto& pair)
        {
            storage = pair.first + pair.second;
            return &storage;
        })
        .where([](const auto sum) { return sum % 3 != 0; })
        .take(3)
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_SUITE_END()
