#include <string>
#include <vector>
#include <optional>
#include <boost/test/unit_test.hpp>

#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Transform)

BOOST_AUTO_TEST_CASE(testTransformIntFromPair)
{
    std::vector<std::pair<int, int> > input {{ 1, 1 },{ 2, 2 },{ 3, 3 }};
    std::vector<int> result, expected { 1, 2, 3 };

    cut::loop(input)
        .transform([](const std::pair<int, int>& pair) {
            return &pair.second;
        })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testTransformIntToString)
{
    std::vector<int> input { 1, 2, 3 };
    std::vector<std::string> result, expected { "1", "2", "3" };

    std::string buffer;
    cut::loop(input)
        .transform([&buffer](int i) {
            buffer = std::to_string(i);
            return &buffer;
        })
        .move_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testMoveCount)
{
    cut::life_log log;

    std::vector<int> dummy { 1, 2, 3 };
    std::vector<cut::life_logger> result;
    result.reserve(3);

    log.start();
    {
        std::optional<cut::life_logger> storage;
        cut::loop(dummy)
            .transform([&log, &storage](const int& i) {
                storage.emplace(log);
                return &storage.value();
            })
            .move_back_to(result);
    }
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 3);
    BOOST_CHECK_EQUAL(log.copy_count, 0);
    BOOST_CHECK_EQUAL(log.move_count, 3);
    BOOST_CHECK_EQUAL(log.dstr_count, 3);
}

BOOST_AUTO_TEST_CASE(testCopyCount)
{
    cut::life_log log;

    std::vector<cut::life_logger> input;
    input.reserve(3);
    input.emplace_back(log);
    input.emplace_back(log);
    input.emplace_back(log);
    std::vector<cut::life_logger> result;
    result.reserve(3);

    log.start();
    cut::loop(input)
        .transform([](const cut::life_logger& ref) {
            return &ref;
        })
        .copy_back_to(result);
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 0);
    BOOST_CHECK_EQUAL(log.copy_count, 3);
    BOOST_CHECK_EQUAL(log.move_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 0);
}

BOOST_AUTO_TEST_SUITE_END()
