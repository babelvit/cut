#include <vector>
#include <string>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Unique)

BOOST_AUTO_TEST_CASE(testUniqueTwoNumbers)
{
    std::vector<std::string> input{ "1", "1", "3", "1", "3", "3" };
    std::vector<std::string> result, expected{ "1", "3" };

    cut::loop(input)
        .unique()
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testUniqueEmptyInput)
{
    std::vector<int> input{ };
    std::vector<int> result, expected{ };

    cut::loop(input)
        .unique()
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testUniqueBySelector)
{
    std::vector<std::string> input{ "1", "1", "3", "1", "3", "3" };
    std::vector<std::string> result, expected{ "1", "3" };

    cut::loop(input)
        .unique([](std::string& str) { return std::stoi(str); })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testCopyCount)
{
    cut::life_log log;

    std::vector<cut::life_logger> input;
    input.reserve(5);
    input.emplace_back(log, 1);
    input.emplace_back(log, 3);
    input.emplace_back(log, 1);
    input.emplace_back(log, 1);
    input.emplace_back(log, 3);

    std::vector<cut::life_logger> result;
    result.reserve(5);

    log.start();
    cut::loop(input)
        .unique()
        .copy_back_to(result);
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 0);
    BOOST_CHECK_EQUAL(log.copy_count, 4); // 2 set, 2 result
    BOOST_CHECK_EQUAL(log.move_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 2); // 2 set
}

BOOST_AUTO_TEST_SUITE_END()
