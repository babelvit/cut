#include <vector>
#include <optional>
#include <boost/test/unit_test.hpp>
#include "cut/loop/loop.hpp"
#include "cut/test_tool/life_logger.hpp"

BOOST_AUTO_TEST_SUITE(Where)

BOOST_AUTO_TEST_CASE(testDivisibleByThree)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { 3, 6 };

    cut::loop(input)
        .where([](int i) { return i % 3 == 0; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testEvenAndFirstSkip)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { 2, 4, 6, 8 };

    cut::loop(input)
        .where([](int i) { return i % 2 == 0; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testEvenAndFirstAccept)
{
    std::vector<int> input { 2, 1, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { 2, 4, 6, 8 };

    cut::loop(input)
        .where([](int i) { return i % 2 == 0; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testEmptyInput)
{
    std::vector<int> input { };
    std::vector<int> result, expected { };

    cut::loop(input)
        .where([](int i) { return i % 2 == 0; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testEliminateAll)
{
    std::vector<int> input { 1, 2, 3, 4, 5, 6, 7, 8 };
    std::vector<int> result, expected { };

    cut::loop(input)
        .where([](int i) { return i % 2 == 0; })
        .where([](int i) { return i % 2 == 1; })
        .copy_back_to(result);

    BOOST_CHECK_EQUAL_COLLECTIONS(result.begin(), result.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(testCopyCount)
{
    cut::life_log log;
    std::vector<cut::life_logger> input;
    input.reserve(5);
    input.emplace_back(log);
    input.emplace_back(log);
    input.emplace_back(log);
    int callCount = 0;

    std::vector<cut::life_logger> result;
    result.reserve(5);

    log.start();
    cut::loop(input)
        .transform([&callCount](const cut::life_logger& rep) {
            ++callCount;
            return &rep;
        })
        .where([](const cut::life_logger& rep) { return &rep != nullptr; })
        .where([](const cut::life_logger& rep) { return &rep != nullptr; })
        .where([](const cut::life_logger& rep) { return &rep != nullptr; })
        .copy_back_to(result);
    log.stop();

    BOOST_CHECK_EQUAL(callCount, 3);
    BOOST_CHECK_EQUAL(log.cstr_count, 0);
    BOOST_CHECK_EQUAL(log.copy_count, 3);
    BOOST_CHECK_EQUAL(log.move_count, 0);
    BOOST_CHECK_EQUAL(log.dstr_count, 0);
}

BOOST_AUTO_TEST_CASE(testWhereDoesNotDoubleCallOfTransform)
{
    cut::life_log log;

    std::vector<int> input { 1, 2, 3 };
    std::vector<cut::life_logger> result;
    result.reserve(5);

    log.start();
    {
        std::optional<cut::life_logger> storage;
        cut::loop(input)
            .transform([&log, &storage](int i) {
                storage.emplace(log);
                return &storage.value();
            })
            .where([](const cut::life_logger& rep) { return &rep != nullptr; })
            .move_back_to(result);
    }
    log.stop();

    BOOST_CHECK_EQUAL(log.cstr_count, 3);
    BOOST_CHECK_EQUAL(log.copy_count, 0);
    BOOST_CHECK_EQUAL(log.move_count, 3);
    BOOST_CHECK_EQUAL(log.dstr_count, 3);
}

BOOST_AUTO_TEST_SUITE_END()
